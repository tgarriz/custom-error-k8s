# Customizar errores 404, 403, 500



## Escenario 1, Tipo k8s: RKE2

En rancher, a diferencia de k8s, no encontre la forma de generar una custom page para que el ingress por default sirva cuando recibe un request que no se puede 
satisfacer.Por lo que hay que generar un snipet en los ingress que redirija al service-error 
Pasos:
 
- Crear un deployment, un service y configmap que con un nginx para que sirva los html customizados
```bash
---
apiVersion: v1
data:
  "404.html": |-
    "<!DOCTYPE html>\n\n<html lang=\"en\" >\n<head>\n  <meta charset=\"UTF-8\">\n
    \ <title>CodePen - Simple 404 Page</title>\n  <meta name=\"viewport\" content=\"width=device-width,
    initial-scale=1\"><link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Lato:100,300'>\n<style>\n*{\n
    \   transition: all 0.6s;\n}\n\nhtml {\n    height: 100%;\n}\n\nbody{\n    font-family:
    'Lato', sans-serif;\n    color: #888;\n    margin: 0;\n}\n\n#main{\n    display:
    table;\n    width: 100%;\n    height: 100vh;\n    text-align: center;\n}\n\n.fof{\n\t
    \ display: table-cell;\n\t  vertical-align: middle;\n}\n\n.fof h1{\n\t  font-size:
    50px;\n\t  display: inline-block;\n\t  padding-right: 12px;\n\t  animation: type
    .5s alternate infinite;\n}\n\n@keyframes type{\n\t  from{box-shadow: inset -3px
    0px 0px #888;}\n\t  to{box-shadow: inset -3px 0px 0px transparent;}\n}\n</style>\n</head>\n<body>\n<!--
    partial:index.partial.html -->\n<div id=\"main\">\n <div class=\"fof\">\n  <h1>Error
    404</h1>\n  <h2>infraestructura@arba.gov.ar</h2>\n </div>\n</div>\n\n</body>\n</html>\n"
  "503.html": |-
    "<!DOCTYPE html>\n\n<html lang=\"en\" >\n<head>\n  <meta charset=\"UTF-8\">\n
    \ <title>CodePen - Simple 500 Page</title>\n  <meta name=\"viewport\" content=\"width=device-width,
    initial-scale=1\"><link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Lato:100,300'>\n<style>\n*{\n
    \   transition: all 0.6s;\n}\n\nhtml {\n    height: 100%;\n}\n\nbody{\n    font-family:
    'Lato', sans-serif;\n    color: #888;\n    margin: 0;\n}\n\n#main{\n    display:
    table;\n    width: 100%;\n    height: 100vh;\n    text-align: center;\n}\n\n.fof{\n\t
    \ display: table-cell;\n\t  vertical-align: middle;\n}\n\n.fof h1{\n\t  font-size:
    50px;\n\t  display: inline-block;\n\t  padding-right: 12px;\n\t  animation: type
    .5s alternate infinite;\n}\n\n@keyframes type{\n\t  from{box-shadow: inset -3px
    0px 0px #888;}\n\t  to{box-shadow: inset -3px 0px 0px transparent;}\n}\n</style>\n</head>\n<body>\n<!--
    partial:index.partial.html -->\n<div id=\"main\">\n <div class=\"fof\">\n  <h1>Error
    500</h1>\n  <h2>infraestructura@arba.gov.ar</h2>\n </div>\n</div>\n\n</body>\n</html>\n"
kind: ConfigMap
metadata:
  name: custom-error-pages
  namespace: kube-system
---
apiVersion: v1
kind: Service
metadata:
  name: nginx-backend-service
  namespace: kube-system
spec:
  ports:
  - port: 80
    protocol: TCP
    targetPort: 80
  selector:
    app: nginx-backend
---
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: nginx-backend
  name: nginx-backend
  namespace: kube-system
spec:
  replicas: 1
  selector:
    matchLabels:
      app: nginx-backend
  template:
    metadata:
      labels:
        app: nginx-backend
    spec:
      containers:
      - image: nginx:latest
        name: nginx-backend
        ports:
        - containerPort: 80
        volumeMounts:
        - mountPath: /usr/share/nginx/html
          name: error-pages
      volumes:
      - configMap:
          name: custom-error-pages
        name: error-pages
---

```
- Posterior habilitamos los snippets en el configmap del ingress-controller con
```bash
apiVersion: v1
data:
  allow-snippet-annotations: 'true'
kind: ConfigMap
metadata:
  name: rke2-ingress-nginx-controller
  namespace: kube-system
```

- Y en cada ingress donde queramos customizar el error agregamos en los annotations  
```bash
    nginx.ingress.kubernetes.io/server-snippet: |
      error_page 503 = /error-503;
      location /error-503 {
          internal;
          proxy_pass http://nginx-backend-service.svc.cluster.local/503.html;
      }
```
Dependiendo el error que querramos customizar

## Escenario 2 Tipo cluster: k8s

- Desplegamos el deployment, el service y configmap.

```bash
---
apiVersion: v1
kind: Service
metadata:
  name: nginx-errors
  namespace: ingress-nginx
  labels:
    app.kubernetes.io/name: nginx-errors
    app.kubernetes.io/part-of: ingress-nginx
spec:
  selector:
    app.kubernetes.io/name: nginx-errors
    app.kubernetes.io/part-of: ingress-nginx
  ports:
  - port: 80
    targetPort: 8080
    name: http
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-errors
  namespace: ingress-nginx
  labels:
    app.kubernetes.io/name: nginx-errors
    app.kubernetes.io/part-of: ingress-nginx
spec:
  replicas: 1
  selector:
    matchLabels:
      app.kubernetes.io/name: nginx-errors
      app.kubernetes.io/part-of: ingress-nginx
  template:
    metadata:
      labels:
        app.kubernetes.io/name: nginx-errors
        app.kubernetes.io/part-of: ingress-nginx
    spec:
      containers:
      - name: nginx-error-server
        image: registry.k8s.io/ingress-nginx/nginx-errors:v20230505@sha256:3600dcd1bbd0d05959bb01af4b272714e94d22d24a64e91838e7183c80e53f7f
        ports:
        - containerPort: 8080

        # Mounting custom error page from configMap
        volumeMounts:
        - name: custom_error_pages
          mountPath: /www

      # Mounting custom error page from configMap
      volumes:
      - name: custom_error_pages
        configMap:
          name: custom_error_pages
          items:
          - key: "404"
            path: "404.html"
          - key: "503"
            path: "503.html"
---
apiVersion: v1
data:
  "404.html": |-
    "<!DOCTYPE html>\n\n<html lang=\"en\" >\n<head>\n  <meta charset=\"UTF-8\">\n
    \ <title>CodePen - Simple 404 Page</title>\n  <meta name=\"viewport\" content=\"width=device-width,
    initial-scale=1\"><link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Lato:100,300'>\n<style>\n*{\n
    \   transition: all 0.6s;\n}\n\nhtml {\n    height: 100%;\n}\n\nbody{\n    font-family:
    'Lato', sans-serif;\n    color: #888;\n    margin: 0;\n}\n\n#main{\n    display:
    table;\n    width: 100%;\n    height: 100vh;\n    text-align: center;\n}\n\n.fof{\n\t
    \ display: table-cell;\n\t  vertical-align: middle;\n}\n\n.fof h1{\n\t  font-size:
    50px;\n\t  display: inline-block;\n\t  padding-right: 12px;\n\t  animation: type
    .5s alternate infinite;\n}\n\n@keyframes type{\n\t  from{box-shadow: inset -3px
    0px 0px #888;}\n\t  to{box-shadow: inset -3px 0px 0px transparent;}\n}\n</style>\n</head>\n<body>\n<!--
    partial:index.partial.html -->\n<div id=\"main\">\n <div class=\"fof\">\n  <h1>Error
    404</h1>\n  <h2>infraestructura@arba.gov.ar</h2>\n </div>\n</div>\n\n</body>\n</html>\n"
  "503.html": |-
    "<!DOCTYPE html>\n\n<html lang=\"en\" >\n<head>\n  <meta charset=\"UTF-8\">\n
    \ <title>CodePen - Simple 500 Page</title>\n  <meta name=\"viewport\" content=\"width=device-width,
    initial-scale=1\"><link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Lato:100,300'>\n<style>\n*{\n
    \   transition: all 0.6s;\n}\n\nhtml {\n    height: 100%;\n}\n\nbody{\n    font-family:
    'Lato', sans-serif;\n    color: #888;\n    margin: 0;\n}\n\n#main{\n    display:
    table;\n    width: 100%;\n    height: 100vh;\n    text-align: center;\n}\n\n.fof{\n\t
    \ display: table-cell;\n\t  vertical-align: middle;\n}\n\n.fof h1{\n\t  font-size:
    50px;\n\t  display: inline-block;\n\t  padding-right: 12px;\n\t  animation: type
    .5s alternate infinite;\n}\n\n@keyframes type{\n\t  from{box-shadow: inset -3px
    0px 0px #888;}\n\t  to{box-shadow: inset -3px 0px 0px transparent;}\n}\n</style>\n</head>\n<body>\n<!--
    partial:index.partial.html -->\n<div id=\"main\">\n <div class=\"fof\">\n  <h1>Error
    500</h1>\n  <h2>infraestructura@arba.gov.ar</h2>\n </div>\n</div>\n\n</body>\n</html>\n"
kind: ConfigMap
metadata:
  name: custom-error-pages
  namespace: ingress-nginx
```
- Habilitamos los snippets y codigo de errores

```bash
kind: ConfigMap
apiVersion: v1
metadata:
  name: ingress-nginx-controller
  namespace: ingress-nginx
data:
  allow-snippet-annotations: 'true'
  custom-http-errors: 404,500,503
```